import React from "react";
import { Button, Card, OverlayTrigger, Tooltip } from "react-bootstrap";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClock } from '@fortawesome/free-solid-svg-icons'

import classes from "./card.module.css";

const share = (
  <Tooltip id="share">
    <strong>Показать Друзелям!</strong>
  </Tooltip>
);

class InfoCards extends React.Component{
  render() {
    return (
      <Card className={classes.cards} style = {{backgroundColor:this.props.color}}>
        <Card.Body>
        <FontAwesomeIcon icon={faClock} size = '4x' style = {{float:'right', color: 'grey'}}/>
          <Card.Title
            style={{
              fontSize: "20px",
              fontWeight: "bolder",
              color: "black",
              fontFamily: "sans-serif",
            }}
          >
            {this.props.title.toUpperCase()}
            

            
          </Card.Title>
          
          <Card.Text>{this.props.description}</Card.Text>
 
        </Card.Body>
      </Card>
    );
  }
}

export default InfoCards;
