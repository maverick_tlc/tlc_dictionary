
import React, { Component, Fragment } from "react";
import { Card, Jumbotron, Col, Row, Button } from "react-bootstrap";
import Navigation from "./navigation"
import Cards from "./card"
import InfoCards from './infoCard'
import data from '../data/data.json';


var legend = [

    {
        "title":"Green Cards",
        
        "description":"Covers annual aggregations."
        ,
        "color":"#9cd6ac"
    },


{
    "title":"Pink Cards",
   
    "description":"Covers biannual aggregations."
    ,
    "color":"#ebc3d6"
},
    {
        "title":"Blue Cards",
       
        "description":"Covers monthly aggregations."
        ,
        "color":"#d3d5eb"
    },
    {
        "title":"White Cards",
       
        "description":"Covers daily aggregations."
        ,
        "color":"white"
    }
]



class Home extends Component{

    state = {
        entries:[1,2,3]
    }

//     componentDidMount(){

//         this.setState({entries:data})


//   }
learnMore = () =>{
    alert('Placeholder!')
}

    render(){

console.log(this.state.entries)
        
        return(
<Fragment>

    <Navigation/>
    <Jumbotron style = {{width:'88%', margin: '2% 6%', backgroundColor:'#95adc5'}}>
  <h1>Welcome to The Policy-Programs Dictionary Portal</h1>
  <p>
    This page serves as a quick and easy access to many data aggregations that the Policy and Programs teams have been accumulating and storing for expedite turnaround for various data requests that our teams have been routinely receiving in the past. 
  </p>
  <p>
    <Button variant="primary" onClick = {this.learnMore}>Learn more</Button>
  </p>
</Jumbotron>
    <Row style = {{width:'90%', margin:'0 5%'}}>
<Col lg={{span:8, order: 1}} xs={{span:12,order:2}} md = {{span:12, order:2}} >
{data.map(i => {
    
    return <Cards title = {i['title']} description = {i['description']} address = {i['address']} color = {i['color']}/>
})}
</Col>
<Col lg={{span:4, order: 2}} xs={{span:12,order:1}} md = {{span:12, order:1}}>
{legend.map(i => {
    
    return <InfoCards title = {i['title']} description = {i['description']} color = {i['color']}/>
})}
</Col>
</Row>
    



        

 

    </Fragment>
        )
    }
}

export default Home